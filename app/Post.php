<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'post', 'body'
    ];

    public function postCount(){
        return DB::table('posts')->count();
    }

}
