<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // get all the users
    public function getAllUsers(){
        // here we make the query builder
        $users = DB::table('users')->get();
        return $users;
    }

    public function getChunkData(){
        // here we use chunking with the user database
        // the chunk method use a closure function
        // to work
        // and you must use the orderBy method to
        // use that
        $user_append = array();
        DB::table('users')->orderBy('created_at')->chunk(5,function($users){
                dd($users);

        });

    }

    // this will return a single row

    public function fetchSingleUser($id){
        // this function will fetch the user
        // base on the name
        // like a name Justine Powlowski
        $search_parameter = 'id';
        $user = DB::table('users')->where($search_parameter,$id)->first();
        return $user;
    }

    // lets return a single column
    // this will fetch a column wise data

    public function fetchSingleUserColumn($id){
        $search_parameter = 'id';
        $user_column = DB::table('users')->where($search_parameter,$id)->pluck('name');
        return $user_column;

    }

    public  function columnArray($column_name){
        $data = DB::table('users')->pluck($column_name)->all();
        return $data;
    }

    // now get a email of the id 3
    public function fetchEmailWithId($id){
        $search_param = 'id';
        $output_param = 'email';
        $data = DB::table('users')->where($search_param,$id)->value($output_param);
        dd($data);
    }
    public function countUser(){
        return $this->count();
    }

}
