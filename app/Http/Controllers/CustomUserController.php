<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\User;

class CustomUserController extends Controller
{
    // this is where we fetch the data
	// this is how you get all the users
	// remember the function will not be in the controller
	// it wil be i t he model
    public function get_all_user(){
    	$user = new User();
    	$users = $user->getAllUsers();
    	return $users;
    }

    public function get_chunk_data(){
        $user = new User();
        $users =  $user->getChunkData();
        dd($users);

    }
    public function get_a_user($id){
        $user = new User();
        $users = $user->fetchSingleUser($id);
        dd($users);
    }

    public function fetch_single_user_column($id){
        $user = new User();
        $users = $user->fetchSingleUserColumn($id);
        dd($users);

    }

    public function column_array($name){
        $user = new User();
        $user->columnArray($name);
        dd($user);
    }

    public function fetch_email_with_id($id){
        $user = new User();
        $user->fetchEmailWithId($id);

}

// this is where use  Aggragate function

public function user_count(){
        $user = new User;
        dd($user->countUser());
}

}
