<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Post;

class QueryBuilderPostUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // you have to first understand 
        // there is relation between the 
        // post and the user
        // and one user can have multiple post
        // so the posts table need the id from the users
        // so first take the id from the users and make 
        // and array
        $faker = \Faker\Factory::create();
        Schema::disableForeignKeyConstraints();
        // this will give use permission
        // to insert data in a relational table
        $users = User::all()->pluck('id')->toArray();
        // we collected every user id and convert to array
        // now truncate the table posts with query builder
        DB::table('posts')->delete();

         for($i=0;$i<=100;$i++){

        	// take a random user 
        	// and add a relationship
        	$user_id = $faker->randomElement($users);
        	DB::table('posts')->insert([
                "post" => Str::random(20),
                "body" => Str::random(50),
                "user_id" => $user_id,
                "created_at" => \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now(),
            ]);


    }
}

}
