<?php

use Illuminate\Database\Seeder;

class FacoryUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// we directly call the class from here
    	App\User::truncate();
        $user_number = 50;
        factory(App\User::class,$user_number)->create();
    }
}
