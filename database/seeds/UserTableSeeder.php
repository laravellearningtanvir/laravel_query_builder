<?php

use Illuminate\Database\Seeder;
use \App\User;
class UserTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // this is the query builder approach
    // and this is the manual approach
    // and now you have to add the class in
    // main seeder file
    public function run()
    {
        // we added the data with the query builder and also the eloquent both
    	// first we truncate the users
    	// we can use the qloquent for that

    	User::truncate(); 

    	$faker = \Faker\Factory::create();

    	// we will use 100 user with the faker library
    	for ($i=0; $i <=100 ; $i++) { 
    		// this is where we write the db query
    		DB::table('users')->insert(array(
    			"name" => $faker->userName,
    			"email" => $faker->email,
    			"password"=> Hash::make('password')
    		));
    	}

    }
}
