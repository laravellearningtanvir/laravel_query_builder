<?php

use Illuminate\Database\Seeder;
use \App\User;
class EloquentUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // we will do the exact same thing using the 
        // eloquent

        // first truncate the user table
        User::truncate();
        $faker = \Faker\Factory::create();
        // then make a loop inside the loop there will be eloquent
        for ($i=0; $i <10 ; $i++) { 
        	// we need to create a instance here first
        	$user = new User;
        	// then we need to add it to the attribute
        	$user->name = $faker->userName;
        	$user->email = $faker->email;
        	$user->password = Hash::make('password');
        	$user->save();
        }


    }
}
