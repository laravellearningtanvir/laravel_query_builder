<?php

use Illuminate\Database\Seeder;

class EloquentPostUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // we will do that using  closure function

        factory(App\User::class,50)->create()->each(function($user){
        	$user->posts()->save(factory(App\Post::class)->make());

        	// this will not woek you may have to add a function
        	// in the usres. cant figure it out for now
        });
    }
}
