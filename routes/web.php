<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/get_all_user','CustomUserController@get_all_user');
Route::get('/get_chunk_user','CustomUserController@get_chunk_data');
Route::get('/get_a_user/{id}','CustomUserController@get_a_user');
Route::get('/get_column_user/{id}','CustomUserController@fetch_single_user_column');
Route::get('/get_list/{name}','CustomUserController@column_array');
Route::get('/get_email/{id}','CustomUserController@fetch_email_with_id');
Route::get('/count_user','CustomUserController@user_count');
Route::get('/count_post','CustomPostController@post_count');
